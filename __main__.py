import sys, neuro

source_lang = "auto"
target_lang = "en"
mirror = "translate.mentality.rip"
text = ""
if len(sys.argv) > 1:
    text = sys.argv[1]

if len(sys.argv) == 1:
    translator = neuro.Neuro(mirror)
    while True:
        msg = input(f"{source_lang} -> {target_lang} @ {mirror}> ")
        if msg == "q":
            quit()
        elif msg.startswith("target: "):
            target_lang = msg.split(": ")[1]
        elif msg.startswith("source: "):
            source_lang = msg.split(": ")[1]
        elif msg.startswith("mirror: "):
            mirror = msg.split(": ")[1]
        else:
            print(translator.translate(msg, target=target_lang, source=source_lang))

    quit()
for arg in sys.argv[2:]:
    if arg.startswith("--mirror:"):
        mirror = arg.split(":")[1]
    if arg.startswith("--target:"):
        target_lang = arg.split(":")[1]
    if arg.startswith("--source:"):
        source_lang = arg.split(":")[1] 

print(neuro.Neuro(mirror).translate(text, target=target_lang, source=source_lang))