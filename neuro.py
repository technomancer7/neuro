import requests, json

class Neuro:
    def __init__(self, mirror = "translate.mentality.rip"):
        self.mirror = mirror

    def translate(self, text, source="auto", target="en"):
        url = f"https://{self.mirror}/translate"
        f = requests.post(url, data = json.dumps({"q": text, "source": source, "target": target}), headers = { "Content-Type": "application/json" }).json()
        #print(f)
        if f.get("error"): return f["error"]
        return f["translatedText"]