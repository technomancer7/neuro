# Neuro
Some stuff for interfacing with [LibreTranslator](https://github.com/LibreTranslate/LibreTranslate).

## CLI
```bash
$ python3 neuro "madre de dios"
$ python3 neuro "some english text" --source:en --target:ru
```

## Library
```py
# A default mirror is used if one isn't supplied;
translator = neuro.Neuro("translate.mentality.rip")
# Source language is optional, defined as "auto"
print(translator.translate("hello, world", target="ru", source="auto"))
```